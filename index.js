const express = require('express')
const app = express()
const port = 3000
const URLBase = '/apimaob'
const version1 = '/v1'

const MLAB_API_KEY = 'apiKey=f-0MojUgsOTWJS-JYabVXWtktKrRopRm';
const URI_BD_MLAB = 'https://api.mlab.com/api/1';

const URI_DE_PERU = 'https://www.deperu.com/api/rest/cotizaciondolar.json';

var bodyParser = require('body-parser')
var requestJson = require('request-json')
var cors = require('cors')

app.use(bodyParser.json());
app.use(cors());

/*app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/


app.listen(port)

var mLabClient = requestJson.createClient(URI_BD_MLAB);
var dePeruClient = requestJson.createClient(URI_DE_PERU);

app.post(URLBase+version1+'/login', function(req, res){
  console.log(req.body);

  let usr = req.body.user;
  let pwd = req.body.password;

  var query = "q={'alias':'"+usr+"','password':'"+pwd+"'}";
  var filter = "f={'_id':0,'password':0}"; //para no incluir el id mlab ni el password

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/users?'+query+"&"+filter+"&"+MLAB_API_KEY, function(err, resM, body) {
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });

});


app.get(URLBase+version1+'/accounts', function(req, res){
  console.log(`GET ${URLBase}${version1}/accounts`);

  let query = "q={'alias':'"+req.headers.alias+"'}";

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/accounts?'+query+"&"+MLAB_API_KEY, function(err, resM, body){
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});

app.get(URLBase+version1+'/accountTransactions/:accountNumber', function(req, res){
  console.log(`GET ${URLBase}${version1}/accountTransactions/`+req.params.accountNumber);

  let query = "q={'alias':'"+req.headers.alias+"','accountNumber':'"+req.params.accountNumber+"'}";

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/accountTransactions?'+query+"&"+MLAB_API_KEY, function(err, resM, body){
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});


app.get(URLBase+version1+'/exchangeRate', function(req, res){
  console.log(`GET ${URLBase}${version1}/exchangeRate`);

  dePeruClient.get(URI_DE_PERU, function(err, resM, body){
    if(!err){
    if(body != null){
        var data = {'compra':body.Cotizacion[0].Compra,'venta':body.Cotizacion[0].Venta};
        res.status(200);
        res.send(data);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});


app.post(URLBase+version1+'/users', function(req, res){
  console.log(`POST ${URLBase}${version1}/users`);

  let data = req.body;

  mLabClient.post(URI_BD_MLAB+'/databases/dbmaob/collections/users?'+MLAB_API_KEY, data, function(err, resM, body) {
    if(!err){
      if(body != null){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});

app.put(URLBase+version1+'/users/:alias', function(req, res){
  console.log(`POST ${URLBase}${version1}/users/`+req.params.alias);

  let data = { "$set" : req.body };
  let query = "q={'alias':'"+req.params.alias+"'}";

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/users?'+query+"&"+MLAB_API_KEY, function(errG, resG, bodyG) {
    if(!errG){
      if(bodyG !=null && bodyG.length > 0){
        mLabClient.put(URI_BD_MLAB+'/databases/dbmaob/collections/users/'+bodyG[0]._id.$oid+"?"+MLAB_API_KEY, data, function(errP, resP, bodyP) {
          if(!errP){
            if(resP.statusCode == 200){
              res.status(200);
              res.send(bodyP);
            }else{
              res.status(204);
              res.send(null);
            }
          }else{
            res.status(500)
            res.send(errP);
          }
        });
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});


app.delete(URLBase+version1+'/users/:alias', function(req, res){
  console.log(`POST ${URLBase}${version1}/users/`+req.params.alias);

  let data = req.body;
  let query = "q={'alias':'"+req.params.alias+"'}";

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/users?'+query+"&"+MLAB_API_KEY, function(err, resM, body) {
    if(!err){
      if(body !=null && body.length > 0){
        mLabClient.del(URI_BD_MLAB+'/databases/dbmaob/collections/users/'+body[0]._id.$oid+"?"+MLAB_API_KEY, function(err, resM, body) {
          if(!err){
            if(resM.statusCode == 200){
              res.status(200);
              res.send(null);
            }else{
              res.status(204);
              res.send(null);
            }
          }else{
            res.status(500)
            res.send(err);
          }
        });
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });
});

app.get(URLBase+version1+'/users/:alias', function(req, res){
  console.log(`GETº ${URLBase}${version1}/users/`+req.params.alias);
  
  let alias = req.params.alias;

  var query = "q={'alias':'"+alias+"'}";
  var filter = "f={'_id':0}"; //para no incluir el id mlab ni el password

  mLabClient.get(URI_BD_MLAB+'/databases/dbmaob/collections/users?'+query+"&"+filter+"&"+MLAB_API_KEY, function(err, resM, body) {
    if(!err){
      if(body.length>0){
        res.status(200);
        res.send(body);
      }else{
        res.status(204);
        res.send(null);
      }
    }else{
      res.status(500)
      res.send(err);
    }
  });

});

app.use(function(req, res, next){
  res.status(404);

  res.format({

    json: function () {
      res.json({ error: 'Resource not found' })
    }

  })
});
