{
	"alias": "L46166359",
	"accounts": [{
		"id": "00110130000105038205",
		"currency": {
			"id": "PEN"
		},
		"product": {
			"name": "CUENTA CORRIENTE"
		},
		"currentBalance": {
			"amount": "728321.42",
			"currency": "PEN"
		},
		"availableBalance": {
			"amount": "728321.42",
			"currency": "PEN"
		}
	},
	{
		"id": "00110220000110005942",
		"currency": {
			"id": "PEN"
		},
		"product": {
			"name": "CUENTA VIP"
		},
		"currentBalance": {
			"amount": "40777.70",
			"currency": "PEN"
		},
		"availableBalance": {
			"amount": "40777.70",
			"currency": "PEN"
		}
	},
	{
		"id": "00110130000201031777",
		"currency": {
			"id": "PEN"
		},
		"product": {
			"name": "CUENTA GANADORA"
		},
		"currentBalance": {
			"amount": "22023.11",
			"currency": "PEN"
		},
		"availableBalance": {
			"amount": "22023.11",
			"currency": "PEN"
		}
	},
	{
		"id": "00110130000201031904",
		"currency": {
			"id": "PEN"
		},
		"product": {
			"name": "CUENTA SUELDO"
		},
		"currentBalance": {
			"amount": "49946.59",
			"currency": "PEN"
		},
		"availableBalance": {
			"amount": "10064.59",
			"currency": "PEN"
		}
	},
	{
		"id": "00110220000210003947",
		"currency": {
			"id": "USD"
		},
		"product": {
			"name": "CUENTA FACIL"
		},
		"currentBalance": {
			"amount": "19913.13",
			"currency": "USD"
		},
		"availableBalance": {
			"amount": "19913.13",
			"currency": "USD"
		}
	}]
}
